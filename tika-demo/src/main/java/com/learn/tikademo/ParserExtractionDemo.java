package com.learn.tikademo;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * 3.3.5、自动解析 —— 使用Parse接口解析
 * 1、传任意类型文件到 Tika ==> Tika 自身检测文件类型；
 * 2、Tika解析器库，根据 文件类型，选择合适的解析器；
 * 3、解析完成后，对文档内容 进行提取，元数据提取；
 */
public class ParserExtractionDemo {
	public static void main(String[] args) throws FileNotFoundException, IOException, SAXException, TikaException {
		// 新建存放各种文件的文件里 files
		File fileDir = new File("files");
		// 如果文件夹路径错误，退出程序
		if (!fileDir.exists()) {
			System.out.println("文件夹不存在，请检查");
			System.exit(0);
		}

		Metadata metadata = new Metadata(); // 创建元数据对象
		Parser parser = new AutoDetectParser();
		ParseContext parseContext = new ParseContext();  // 自动检测分词器

		// 获取文件夹下的所有文件，存放在File数组中
		FileInputStream inputStream = null;
		File[] fileArr = fileDir.listFiles();
		for (File f : fileArr) {
			inputStream = new FileInputStream(f);
			BodyContentHandler handler = new BodyContentHandler();  // 创建内容处理器对象
			parser.parse(inputStream, handler, metadata, parseContext);
			System.out.println("ParserExtractionDemo-" + f.getName() + ":\n" + handler.toString());
		}
	}
}
