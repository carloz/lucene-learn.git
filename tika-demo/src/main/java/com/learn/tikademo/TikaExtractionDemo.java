package com.learn.tikademo;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;

import java.io.File;
import java.io.IOException;

/**
 * 3.3.5、自动解析 —— 使用Tika对象解析
 * 1、传任意类型文件到 Tika ==> Tika 自身检测文件类型；
 * 2、Tika解析器库，根据 文件类型，选择合适的解析器；
 * 3、解析完成后，对文档内容 进行提取，元数据提取；
 */
public class TikaExtractionDemo {
	public static void main(String[] args) throws IOException, TikaException {
		Tika tika = new Tika();
		// 新建存放各种文件的文件里 files
		File fileDir = new File("files");
		// 如果文件夹路径错误，退出程序
		if (!fileDir.exists()) {
			System.out.println("文件夹不存在，请检查");
			System.exit(0);
		}
		// 获取文件夹下的所有文件，存放在File数组中
		File[] fileArr = fileDir.listFiles();
		String fileContent;
		for (File f : fileArr) {
			fileContent = tika.parseToString(f); // 自动解析
			System.out.println("Extracted Content: " + fileContent);
		}
	}
}
