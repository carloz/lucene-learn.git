package com.learn.tikademo;

import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.asm.ClassParser;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.tika.parser.microsoft.ooxml.OOXMLParser;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.parser.txt.TXTParser;
import org.apache.tika.parser.xml.XMLParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import java.io.*;

/**
 * 3.3.4 Tika提供PDF文件内容
 */
public class TikaParsePdfDemo {
	public static void main(String[] args) throws FileNotFoundException, IOException, TikaException, SAXException {
		// 文件路径
		String filepath = "files/IKAnalyzer中文分词器V2012使用手册.pdf";
		// 新建File对象
		File pdfFile = new File(filepath);
		// 创建 内容处理器对象
		BodyContentHandler handler = new BodyContentHandler();
		// 创建 元数据对象
		Metadata metadata = new Metadata();
		// 读入文件
//		FileInputStream inputStream = new FileInputStream(pdfFile);
		InputStream inputStream = TikaInputStream.get(pdfFile);
		// 创建解析器对象
		ParseContext parseContext = new ParseContext();
		// 实例化 PDFParser 对象
		PDFParser pdfParser = new PDFParser();
		OOXMLParser ooxmlParser = new OOXMLParser();
		TXTParser txtParser = new TXTParser();
		HtmlParser htmlParser = new HtmlParser();
		XMLParser xmlParser = new XMLParser();
		ClassParser classParser = new ClassParser();
		// 调用 parse() 方法解析文件
		pdfParser.parse(inputStream, handler, metadata, parseContext);
		// 遍历元数据内容
		System.out.println("文件属性信息");
		for (String name : metadata.names()) {
			System.out.println(name + ":" + metadata.get(name));
		}
		// 打印 pdf 文件中的内容
		System.out.println("pdf 文件中的内容：");
		System.out.println(handler.toString());

	}
}
