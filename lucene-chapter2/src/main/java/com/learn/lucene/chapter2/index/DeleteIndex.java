package com.learn.lucene.chapter2.index;

import com.learn.lucene.chapter2.ik.IKAnalyzer8x;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 删除索引
 * 2.4.4、索引的删除
 * 索引同样存在 CRUD 操作
 * 本节演示 根据 Term 来删除点单个或多个Document，删除 title 中 包含关键词“美国”的文档。
 */
public class DeleteIndex {
	public static void main(String[] args) {
		// 删除 title 中含有关键字“美国”的文档
		deleteDoc("title", "美国");
	}

	public static void deleteDoc(String field, String key) {
		Analyzer analyzer = new IKAnalyzer8x();
		IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
		Path indexPath = Paths.get("indexdir");
		Directory directory;
		try {
			directory = FSDirectory.open(indexPath);
			IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig);
			indexWriter.deleteDocuments(new Term(field, key));
			indexWriter.commit();
			indexWriter.close();
			System.out.println("删除完成");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
