package com.learn.lucene.chapter2.analyzer;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.io.StringReader;

/**
 * 2.3.2、分词测试
 * StandardAnalyzer 分词器测试
 */
public class StdAnalyzerDemo {
	private static String strCh = "中华人名共和国简称中国，是一个有13亿人口的国家";
	private static String strEn = "Dogs can not achieve a place, eyes can reach;";
	public static void main(String[] args) throws IOException {
		System.out.println("StandardAnalyzer 对中文分词：");
		stdAnalyzer(strCh);
		System.out.println("StandardAnalyzer 对英文分词：");
		stdAnalyzer(strEn);
	}

	public static void stdAnalyzer(String str) throws IOException {
		Analyzer analyzer = null;
		analyzer = new StandardAnalyzer();
		StringReader reader = new StringReader(str);
		TokenStream tokenStream = analyzer.tokenStream(str, reader);
		tokenStream.reset();
		CharTermAttribute charTermAttribute = tokenStream.getAttribute(CharTermAttribute.class);
		System.out.println("分词结果：");
		while (tokenStream.incrementToken()) {
			System.out.print(charTermAttribute.toString() + "|");
		}
		System.out.println("\n");
		analyzer.close();
	}
}
