package com.learn.lucene.chapter2.queries;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 2.5.4、布尔搜索（BooleanQuery）
 * 查询 content 中包含美国，并且 title 不包含美国的文档；
 */
public class BooleanQueryTest {
	public static void main(String[] args) throws IOException, ParseException {
		Path indexPath = Paths.get("indexdir");
		Directory directory = FSDirectory.open(indexPath);
		IndexReader reader = DirectoryReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(reader);
		TermQuery query1 = new TermQuery(new Term("title", "美国"));
		TermQuery query2 = new TermQuery(new Term("content", "美国"));
		BooleanClause booleanClause1 = new BooleanClause(query1, BooleanClause.Occur.MUST_NOT);
		BooleanClause booleanClause2 = new BooleanClause(query2, BooleanClause.Occur.MUST);
		BooleanQuery query = new BooleanQuery.Builder().add(booleanClause1).add(booleanClause2).build();

		System.out.println("Query: " + query.toString());  // 查询关键词
		// 返回前10条
		TopDocs topDocs = searcher.search(query, 10);
		for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
			Document doc = searcher.doc(scoreDoc.doc);
			System.out.println("DocID: " + scoreDoc.doc);
			System.out.println("id: " + doc.get("id"));
			System.out.println("title: " + doc.get("title"));
			System.out.println("content: " + doc.get("content"));
			System.out.println("文档评分: " + scoreDoc.score);
		}
		directory.close();
		reader.close();
	}

}
