package com.learn.lucene.chapter2.queries;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 2.5.6、前缀搜索（PrefixQuery）
 * 举例：搜索 包含以“学”开头的词项 的文档
 */
public class PrefixQueryTest {
	public static void main(String[] args) throws IOException, ParseException {

		Path indexPath = Paths.get("indexdir");
		Directory directory = FSDirectory.open(indexPath);
		IndexReader reader = DirectoryReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(reader);
		Term term = new Term("title", "学");
		Query query = new PrefixQuery(term);
		System.out.println("Query: " + query.toString());  // 查询关键词
		// 返回前10条
		TopDocs topDocs = searcher.search(query, 10);
		for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
			Document doc = searcher.doc(scoreDoc.doc);
			System.out.println("DocID: " + scoreDoc.doc);
			System.out.println("id: " + doc.get("id"));
			System.out.println("title: " + doc.get("title"));
			System.out.println("文档评分: " + scoreDoc.score);
		}
		directory.close();
		reader.close();
	}
}
