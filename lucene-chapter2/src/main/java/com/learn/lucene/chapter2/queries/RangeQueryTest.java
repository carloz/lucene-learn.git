package com.learn.lucene.chapter2.queries;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 2.5.5、范围搜索（RangeQuery）
 * 举例：查询新闻回复条数在 500~1000 之间的文档
 */
public class RangeQueryTest {
	public static void main(String[] args) throws IOException, ParseException {

		Path indexPath = Paths.get("indexdir");
		Directory directory = FSDirectory.open(indexPath);
		IndexReader reader = DirectoryReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(reader);
		Query query = IntPoint.newRangeQuery("reply", 500, 1000);
		System.out.println("Query: " + query.toString());  // 查询关键词
		// 返回前10条
		TopDocs topDocs = searcher.search(query, 10);
		for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
			Document doc = searcher.doc(scoreDoc.doc);
			System.out.println("DocID: " + scoreDoc.doc);
			System.out.println("id: " + doc.get("id"));
			System.out.println("title: " + doc.get("title"));
			System.out.println("content: " + doc.get("content"));
			System.out.println("reply: " + doc.get("reply_display"));
			System.out.println("文档评分: " + scoreDoc.score);
		}
		directory.close();
		reader.close();
	}
}
