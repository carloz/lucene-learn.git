package com.learn.lucene.chapter2.queries;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 2.5.7、多关键字搜索（PhraseQuery）
 * PhraseQuery 可以 通过add方法添加多个关键字
 * 还可以通过 setSlop() 设定“坡度”，允许关键字之间 无关词汇存在量
 */
public class PhraseQueryTest {
	public static void main(String[] args) throws IOException, ParseException {
		String str = "习近平会见奥巴马，学习国外经验";

		Path indexPath = Paths.get("indexdir");
		Directory directory = FSDirectory.open(indexPath);
		IndexReader reader = DirectoryReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(reader);
		PhraseQuery.Builder builder = new PhraseQuery.Builder();
		builder.add(new Term("title", "奥巴马"), str.indexOf("奥巴马"));
		builder.add(new Term("title", "学习国外经验"), str.indexOf("学习国外经验"));
		PhraseQuery query = builder.build();
		System.out.println("Query: " + query.toString());  // 查询关键词
		// 返回前10条
		TopDocs topDocs = searcher.search(query, 10);
		for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
			Document doc = searcher.doc(scoreDoc.doc);
			System.out.println("DocID: " + scoreDoc.doc);
			System.out.println("id: " + doc.get("id"));
			System.out.println("title: " + doc.get("title"));
			System.out.println("content: " + doc.get("content"));
			System.out.println("reply: " + doc.get("reply_display"));
			System.out.println("文档评分: " + scoreDoc.score);
		}
		directory.close();
		reader.close();
	}
}
