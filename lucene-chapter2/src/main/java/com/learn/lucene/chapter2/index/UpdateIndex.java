package com.learn.lucene.chapter2.index;

import com.learn.lucene.chapter2.ik.IKAnalyzer8x;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 更新索引
 * 2.4.5 索引的更新
 * 本质：先删除索引，再建立新的文档。
 */
public class UpdateIndex {

	public static void main(String[] args) {
		Analyzer analyzer = new IKAnalyzer8x();
		IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
		Path indexPath = Paths.get("indexdir");
		Directory directory;
		try {
			directory = FSDirectory.open(indexPath);
			IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig);
			Document doc = new Document();

			// 设置新闻ID 索引 并存储
			FieldType idType = new FieldType();
			idType.setIndexOptions(IndexOptions.DOCS);
			idType.setStored(true);
			// 设置新闻标题索引文档，词项频率，位移信息，和偏移量，存储并词条化
			FieldType titleType = new FieldType();
			titleType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
			titleType.setStored(true);
			titleType.setTokenized(true);

			FieldType contentType = new FieldType();
			contentType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
			contentType.setStored(true);
			contentType.setTokenized(true);
			contentType.setStoreTermVectors(true);
			contentType.setStoreTermVectorPositions(true);
			contentType.setStoreTermVectorOffsets(true);
			contentType.setStoreTermVectorPayloads(true);

			doc.add(new Field("id", "2", idType));
			doc.add(new Field("title", "北大迎4380名新生", titleType));
			doc.add(new Field("content", "昨天，北京大学迎来4380名来自全国各地及数十个国家的本科新生。其中，农村学生工700余名，为今年最多...", contentType));
			indexWriter.updateDocument(new Term("title", "北大"), doc);
			indexWriter.commit();
			indexWriter.close();
			System.out.println("更新完成");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
