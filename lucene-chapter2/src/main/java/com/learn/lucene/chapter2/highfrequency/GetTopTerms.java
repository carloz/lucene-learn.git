package com.learn.lucene.chapter2.highfrequency;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

/**
 * 提取高频词
 */
public class GetTopTerms {
	public static void main(String[] args) throws IOException {
		Directory directory = FSDirectory.open(Paths.get("indexdir"));
		IndexReader reader = DirectoryReader.open(directory);
		// 因为之索引了一个文档，所以DocID为0
		// 通过 getTermVector 获取 content 字段的词项
		Terms terms = reader.getTermVector(0, "content");
		// 遍历词项
		TermsEnum termsEnum = terms.iterator();
		Map<String, Integer> map = new HashMap<String, Integer>();
		BytesRef thisTerm;
		while (null != (thisTerm = termsEnum.next())) {
			String termText = thisTerm.utf8ToString();  // 词项
			// 通过 totalTermFreq() 方法获取词项频率
			map.put(termText, (int) termsEnum.totalTermFreq());
		}
		// 按 value 排序
		List<Map.Entry<String, Integer>> sortedMap = new ArrayList<>(map.entrySet());
		Collections.sort(sortedMap, new Comparator<Map.Entry<String, Integer>>() {
			@Override
			public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
				return (o2.getValue() - o1.getValue());
			}
		});

		getTopN(sortedMap, 10);
	}

	public static void getTopN(List<Map.Entry<String, Integer>> sortedMap, int N) {
		for (int i = 0; i < N; i++) {
			System.out.println(sortedMap.get(i).getKey() + ":" + sortedMap.get(i).getValue());
		}
	}
}
