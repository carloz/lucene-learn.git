package com.learn.lucene.chapter2.analyzer;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.cjk.CJKAnalyzer;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.io.StringReader;
import java.util.StringJoiner;

/**
 * 测试多种分词器
 */
public class VariousAnalyzersDemo {

	private static String strCh = "中华人民共和国简称中国，是一个有13亿人口的国家";
	private static String strEn = "Dogs can not achieve a place, eyes can reach;";

	public static void main(String[] args) throws IOException {
		System.out.println("标准分词：" + printAnalyzer(new StandardAnalyzer(), strCh));
		System.out.println("空格分词：" + printAnalyzer(new WhitespaceAnalyzer(), strCh));
		System.out.println("简单分词：" + printAnalyzer(new SimpleAnalyzer(), strCh));
		System.out.println("二分法分词：" + printAnalyzer(new CJKAnalyzer(), strCh));
		System.out.println("关键字分词：" + printAnalyzer(new KeywordAnalyzer(), strCh));
		System.out.println("停用词分词：" + printAnalyzer(new StopAnalyzer(new StringReader(strCh)), strCh));
		System.out.println("中文智能分词：" + printAnalyzer(new SmartChineseAnalyzer(), strCh));
	}

	public static String printAnalyzer(Analyzer analyzer, String str) throws IOException {
		StringReader reader = new StringReader(str);
		TokenStream tokenStream = analyzer.tokenStream(str, reader);
		tokenStream.reset();
		CharTermAttribute charTermAttribute = tokenStream.getAttribute(CharTermAttribute.class);
		StringJoiner stringJoiner = new StringJoiner("|");
		while (tokenStream.incrementToken()) {
			stringJoiner.add(charTermAttribute.toString());
		}
		analyzer.close();
		return stringJoiner.toString();
	}

}
