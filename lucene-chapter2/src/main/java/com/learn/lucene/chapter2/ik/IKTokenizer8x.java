package com.learn.lucene.chapter2.ik;

import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

import java.io.IOException;

/**
 * 修改 IKTokenizer —— incrementToken()
 */
public class IKTokenizer8x extends Tokenizer {
	// IK分词器实现
	private IKSegmenter _IKIkSegmenter;
	// 词元文本属性
	private final CharTermAttribute termAttribute;
	// 词元位移属性
	private final OffsetAttribute offsetAttribute;
	// 词元分类属性
	// (该属性分类参考 org.wltea.analyzer.core.Lexeme 中的分类常量)
	private final TypeAttribute typeAttribute;
	// 记录最后一个词元的结束位置
	private int endPosttion;

	// Lucene 8.x Tokenizer适配器类构造函数，实现最新的 Tokenizer 接口
	public IKTokenizer8x(boolean useSmart) {
		super();
		offsetAttribute = addAttribute(OffsetAttribute.class);
		termAttribute = addAttribute(CharTermAttribute.class);
		typeAttribute = addAttribute(TypeAttribute.class);
		_IKIkSegmenter = new IKSegmenter(input, useSmart);
	}

	@Override
	public boolean incrementToken() throws IOException {
		clearAttributes();      // 清除所有的词元属性
		Lexeme nextLexeme = _IKIkSegmenter.next();
		if (nextLexeme != null) {
			// 将 Lexeme 转化成 Attributes
			termAttribute.append(nextLexeme.getLexemeText());  // 设置词元文本
			termAttribute.setLength(nextLexeme.getLength());  // 设置词元长度
			offsetAttribute.setOffset(nextLexeme.getBeginPosition(), nextLexeme.getEndPosition());  // 设置词元位移
			endPosttion = nextLexeme.getEndPosition();  // 记录 分词的最后位置
			typeAttribute.setType(nextLexeme.getLexemeText()); // 记录词元分分类
			return true; // 返回true 告知 还有下个词元
		}
		return false;
	}

	@Override
	public void reset() throws IOException {
		super.reset();
		_IKIkSegmenter.reset(input);
	}

	@Override
	public final void end() throws IOException {
		int finalOffset = correctOffset(this.endPosttion);
		offsetAttribute.setOffset(finalOffset, finalOffset);
	}
}
