package com.learn.lucene.chapter2.analyzer;

import com.learn.lucene.chapter2.ik.IKAnalyzer8x;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.io.StringReader;
import java.util.StringJoiner;

/**
 * 2.3.4、中文分词器效果对比
 * 1、Lucene 自带的 SmartChineseAnalyzer 分词器
 * 2、IKAnalyzer8x分词器
 */
public class IkVSSmartchDemo {

	private static String str1 = "公路局正在治理解放大道路面积水问题。";
	private static String str2 = "IKAnalyzer 是一个开源的，基于java语言开发的轻量级的中文分词工具包。";

	public static void main(String[] args) throws IOException {
		System.out.println("句子一：" + str1);
		System.out.println("SmartChineseAnalyzer分词结果：" + printAnalyzer(new SmartChineseAnalyzer(), str1));
		System.out.println("IKAnalyzer8x分词结果：" + printAnalyzer(new IKAnalyzer8x(true), str1));
//		System.out.println("IKAnalyzer分词结果(bug)：" + printAnalyzer(new IKAnalyzer(), str1));
		System.out.println("----------------------------------------");
		System.out.println("句子二：" + str2);
		System.out.println("SmartChineseAnalyzer分词结果：" + printAnalyzer(new SmartChineseAnalyzer(), str2));
		System.out.println("IKAnalyzer8x分词结果：" + printAnalyzer(new IKAnalyzer8x(true), str2));
//		System.out.println("IKAnalyzer分词结果(bug)：" + printAnalyzer(new IKAnalyzer(true), str2));
	}

	public static String printAnalyzer(Analyzer analyzer, String str) throws IOException {
		StringReader reader = new StringReader(str);
		TokenStream tokenStream = analyzer.tokenStream(str, reader);
		tokenStream.reset();
		CharTermAttribute charTermAttribute = tokenStream.getAttribute(CharTermAttribute.class);
		StringJoiner stringJoiner = new StringJoiner("|");
		while (tokenStream.incrementToken()) {
			stringJoiner.add(charTermAttribute.toString());
		}
		analyzer.close();
		return stringJoiner.toString();
	}
}
