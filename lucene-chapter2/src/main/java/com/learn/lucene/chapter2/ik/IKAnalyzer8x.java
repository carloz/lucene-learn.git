package com.learn.lucene.chapter2.ik;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Tokenizer;

/**
 * 2.3.3、IK分词器配置
 * 修改IKAnalyzer —— createComponents(String fieldName)
 */
public class IKAnalyzer8x extends Analyzer {

	private boolean useSmart;

	public IKAnalyzer8x() {
		this(false);
	}

	public IKAnalyzer8x(boolean useSmart) {
		super();
		this.useSmart = useSmart;
	}

	@Override
	protected TokenStreamComponents createComponents(String fieldName) {
		Tokenizer _IKTokenizer = new IKTokenizer8x(this.isUseSmart());
		return new TokenStreamComponents(_IKTokenizer);
	}

	public boolean isUseSmart() {
		return useSmart;
	}

	public void setUseSmart(boolean useSmart) {
		this.useSmart = useSmart;
	}
}
