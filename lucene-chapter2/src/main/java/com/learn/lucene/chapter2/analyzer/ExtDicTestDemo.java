package com.learn.lucene.chapter2.analyzer;

import com.learn.lucene.chapter2.ik.IKAnalyzer8x;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.io.StringReader;
import java.util.StringJoiner;

/**
 * 2.3.5、扩展停用词词典
 * 2.3.6、扩展自定义词典
 * ext.dic 测试
 */
public class ExtDicTestDemo {

	private static String str = "厉害了我的哥！中国环保部门即将发布治理北京雾霾的方法！";

	public static void main(String[] args) throws IOException {
		Analyzer analyzer = new IKAnalyzer8x(true);
		StringReader reader = new StringReader(str);
		TokenStream tokenStream = analyzer.tokenStream(str, reader);
		tokenStream.reset();
		CharTermAttribute termAttribute = tokenStream.getAttribute(CharTermAttribute.class);
		System.out.println("分词结果：");
		StringJoiner stringJoiner = new StringJoiner("|");
		while (tokenStream.incrementToken()) {
			stringJoiner.add(termAttribute.toString());
		}
		System.out.println(stringJoiner.toString());
		analyzer.close();
	}
}
