package com.learn.lucenefilesearch.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;
import java.nio.charset.Charset;

/**
 * 文件下载
 * 访问：http://localhost:18080/download?fileName=Mycat-config.xml
 */
@Slf4j
@Controller
public class FileDownloadServletController {

	@RequestMapping("/download")
	public ResponseEntity<FileSystemResource> downloadFile(String fileName) throws Exception {
		String filePath = "files/" + fileName;
		File file = new File(filePath);
		log.info("download path:" + file.getPath());
		FileSystemResource fileSystemResource = new FileSystemResource(file);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		headers.setContentLength(file.length());
		headers.setCacheControl(CacheControl.noStore());
		headers.setContentDisposition(
				ContentDisposition.builder("attachment")
				.filename(fileName, Charset.defaultCharset())
				.size(file.length())
				.build()
		);
		return ResponseEntity.ok()
				.headers(headers)
				.body(fileSystemResource);
	}
}
