package com.learn.lucenefilesearch.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 文件对象
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FileModel {
	private String filename; // 文件名
	private String title; // 文件标题
	private String content;  // 文件内容
}
