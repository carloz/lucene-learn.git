package com.learn.lucenefilesearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LuceneFilesearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(LuceneFilesearchApplication.class, args);
	}

}
