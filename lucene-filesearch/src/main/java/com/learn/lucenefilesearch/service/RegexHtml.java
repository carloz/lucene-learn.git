package com.learn.lucenefilesearch.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 将结果中的html标签过滤掉
 */
public class RegexHtml {

	public static String delHtmlTag(String line) {
		if (null == line) return null;
		String regEx_html = "<[^>]+>";
		Pattern pattern = Pattern.compile(regEx_html);
		Matcher matcher = pattern.matcher(line);
		line = matcher.replaceAll("");
		return line;
	}

	public static void main(String[] args) {
		System.out.println(RegexHtml.delHtmlTag("<span style=\"color:red;\">session</span>共享版上线（第二版）.pptx"));
	}
}
