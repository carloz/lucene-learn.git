package com.learn.lucenefilesearch.service;

import com.learn.lucenefilesearch.model.FileModel;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 对 webapp/files 下的文档生成索引，保存在webapp/indexdir中
 */
public class CreateIndex {

	public static void main(String[] args) throws IOException {
		Analyzer analyzer = new IKAnalyzer8x(true);
		IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
		indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		Directory directory = null;
		IndexWriter indexWriter = null;
		Path indexPath = Paths.get("indexdir");
		FieldType fieldType = new FieldType();
		fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
		fieldType.setStored(true);
		fieldType.setTokenized(true);
		fieldType.setStoreTermVectors(true);
		fieldType.setStoreTermVectorPositions(true);
		fieldType.setStoreTermVectorOffsets(true);
		Date start = new Date();
		if (!Files.isReadable(indexPath)) {
			System.out.println(indexPath.toAbsolutePath() + "不存在或不可读，请检查");
			System.exit(1);
		}
		directory = FSDirectory.open(indexPath);
		indexWriter = new IndexWriter(directory, indexWriterConfig);
		ArrayList<FileModel> fileModelList = (ArrayList<FileModel>) extractFile();
		for (FileModel f : fileModelList) {
			Document doc = new Document();
			doc.add(new Field("title", f.getTitle(), fieldType));
			doc.add(new Field("content", f.getContent(), fieldType));
			indexWriter.addDocument(doc);
		}
		indexWriter.commit();
		indexWriter.close();
		directory.close();
		Date end = new Date();
		System.out.println("索引文档完成，共耗时：" + (end.getTime() - start.getTime()) + " 毫秒。");
	}

	public static List<FileModel> extractFile() throws IOException {
		ArrayList<FileModel> list = new ArrayList<>();
		File fileDir = new File("files");
		File[] allFiles = fileDir.listFiles();
		for (File f : allFiles) {
			FileModel fm = new FileModel(f.getName(), f.getName(), ParserExtraction(f));
			list.add(fm);
		}
		return list;
	}

	public static String ParserExtraction(File file) {
		String fileContent = "";
		BodyContentHandler handler = new BodyContentHandler();
		Parser parser = new AutoDetectParser(); // 自动解析器接口
		Metadata metadata = new Metadata();
		FileInputStream inputStream;
		try {
			inputStream = new FileInputStream(file);
			ParseContext context = new ParseContext();
			parser.parse(inputStream, handler, metadata, context);
			fileContent = handler.toString();
			inputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (TikaException e) {
			e.printStackTrace();
		}
		return fileContent;
	}
}
